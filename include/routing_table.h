#pragma once
#include <arpa/inet.h>
#include <stdint.h>

typedef struct route_entry_t route_entry;

struct route_entry_t {
    struct in_addr prefix;
    struct in_addr next_hop;
    struct in_addr mask;
    int interface;
};

int routing_table_init(const char* filename);
const route_entry* get_best_route(struct in_addr dest);
