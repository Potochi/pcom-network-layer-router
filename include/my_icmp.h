#pragma once

#include "skel.h"

typedef struct icmp_config_T {
    struct in_addr daddr;
    struct in_addr saddr;
    uint8_t type;
    uint8_t code;
    uint16_t id;
    uint16_t sequence;
    uint8_t* data;
    size_t data_len;
    int interface;
} icmp_config;

typedef struct packet_headers_T {
    struct ether_header* eth_header;
    struct iphdr* ip_header;
    struct icmphdr* icmp_header;
    struct arp_header* arp_header;
} packet_headers;

void liv_reply_to_echo(packet* p);
void parse_icmp_config_from_header(packet* p, icmp_config* config);
void liv_send_icmp(const icmp_config* const cfg);
packet_headers parse_icmp_packet_headers(const packet* const p);
