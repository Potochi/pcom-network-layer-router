#pragma once
#include "routing_table.h"
#include "skel.h"

/**
 * @brief liv_send_packet send packet
 * @param p packet
 * @param best_route best route
 */
void liv_send_packet(packet* p, const route_entry* const best_route);
