#include "routing_table.h"
#include "liblog.h"
#include <arpa/inet.h>
#include <errno.h>
#include <stdint.h>
#include <stdio.h>

typedef struct routing_table_t routing_table;

#define MSBDEF(name, type)                                                     \
    static type name##_msb(type x) {                                           \
        return (x & ((type)1 << (sizeof(type) * 8 - 1))) >>                    \
               (sizeof(type) * 8 - 1);                                         \
    }

MSBDEF(u32, uint32_t);

routing_table* rtable;

struct trie_entry_t;
typedef struct trie_entry_t trie_entry;

struct trie_entry_t {
    struct trie_entry_t* entries[2];
    route_entry* data;
};

struct routing_table_t {
    uint32_t initialized;
    uint32_t locked;
    trie_entry root;
};

typedef struct __attribute__((packed)) uaddr_inet_t {
    union {
        struct in_addr in_addr;
        uint32_t addr;
        uint8_t bytes[4];
    };
} uaddr_inet;

static void insert_route(route_entry* const entry) {

    assert_true(
        rtable->initialized == 1,
        "Trying to add route to uninitialized routing table. Aborting...");

    assert_true(rtable->locked == 0,
                "Trying to add route to locked routing table. Aborting...");

    uint32_t mask = ntohl(entry->mask.s_addr);
    uint32_t prefix = ntohl(entry->prefix.s_addr);

    trie_entry* it = &rtable->root;

    assert_true(it != NULL, "Routing trie root is null. Aborting...");

    while (mask) {
        uint32_t prefix_msb = u32_msb(prefix);

        if (it->entries[prefix_msb] == NULL) {
            it->entries[prefix_msb] = calloc(1, sizeof(trie_entry));
            assert_true(it->entries[prefix_msb] != NULL,
                        "Failed to allocate memory for routing trie entry. "
                        "Aborting...");
        }
        it = it->entries[prefix_msb];
        mask <<= 1;
        prefix <<= 1;
    }
    it->data = entry;
}

int routing_table_init(const char* filename) {

    rtable = calloc(1, sizeof(routing_table));
    assert_true(rtable != NULL,
                "Failed to allocate memory for routing table. Aborting...");

    rtable->initialized = 1;
    rtable->locked = 0;
    rtable->root.entries[0] = NULL;
    rtable->root.entries[1] = NULL;

    INFO("Parsing routing table %s...", filename);

    FILE* routing_data_file = fopen(filename, "r");

    assert_true(routing_data_file != NULL,
                "Failed to open routing file. Aborting...");

    int read_result;

    uaddr_inet prefix;
    uaddr_inet next_hop;
    uaddr_inet mask;
    int interface;

    while (1) {
        read_result = fscanf(
            routing_data_file,
            "%hhu.%hhu.%hhu.%hhu %hhu.%hhu.%hhu.%hhu %hhu.%hhu.%hhu.%hhu "
            "%d\n",
            &prefix.bytes[0], &prefix.bytes[1], &prefix.bytes[2],
            &prefix.bytes[3], &next_hop.bytes[0], &next_hop.bytes[1],
            &next_hop.bytes[2], &next_hop.bytes[3], &mask.bytes[0],
            &mask.bytes[1], &mask.bytes[2], &mask.bytes[3], &interface);

        assert_true(read_result >= 0 || read_result == EOF,
                    "Read failed on routing file before EOF. Aborting...");

        if (read_result != 13) {
            INFO("%s", "Parsing file EOF...");
            break;
        }
#ifdef LOG_RTABLE_ENTRIES
        LOG("%s", "Adding route:");
        LOG("\tprefix: %s", inet_ntoa(prefix.in_addr));
        LOG("\tnext_hop: %s", inet_ntoa(next_hop.in_addr));
        LOG("\tmask: %s", inet_ntoa(mask.in_addr));
        LOG("\tinerface: %u", interface);
#endif
        route_entry* new_entry = calloc(1, sizeof(*new_entry));

        assert_true(new_entry != NULL,
                    "Failed to allocate memory for routing table entry.");

        new_entry->prefix.s_addr = prefix.addr;
        new_entry->next_hop.s_addr = next_hop.addr;
        new_entry->mask.s_addr = mask.addr;
        new_entry->interface = interface;

        insert_route(new_entry);
    }

    rtable->locked = 1;

    INFO("Parsing routing table %s done.", filename);
    fclose(routing_data_file);
    return 0;
}

const route_entry* get_best_route(struct in_addr dest) {

    INFO("Searching best next-hop for destination %s...",
         inet_ntoa(*(struct in_addr*)&dest));

    uint32_t destination = ntohl(dest.s_addr);
    trie_entry* it = &rtable->root;

    while (1) {
        uint32_t dest_msb = u32_msb(destination);
        if (it->entries[dest_msb] == NULL) {
            break;
        }
        it = it->entries[dest_msb];
        destination <<= 1;
    }
#ifdef LOG_RTABLE_QUERIES
    if (it->data == NULL) {
        LOG("Failed to find route for destination %s", inet_ntoa(dest));
    } else {
        LOG("Found the best route for %s", inet_ntoa(dest));
        LOG("\tprefix: %s", inet_ntoa(it->data->prefix));
        LOG("\tnext_hop: %s", inet_ntoa(it->data->next_hop));
        LOG("\tmask: %s", inet_ntoa(it->data->mask));
        LOG("\tinerface: %u", it->data->interface);
    }
#endif

    return it->data;
}

/*
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
