#include "my_icmp.h"
#include "liblog.h"
#include "skel.h"
#include <arp_table.h>
#include <arpa/inet.h>
#include <router.h>
#include <routing_table.h>
#include <stdint.h>
#include <time.h>

#define IP_HDR_OFFSET (sizeof(struct ether_header))
#define ARP_HDR_OFFSET (sizeof(struct ether_header))
#define ICMP_HDR_OFFSET (sizeof(struct ether_header) + sizeof(struct iphdr))
#define ETH_HDR_OFFSET 0

/**
 * @brief Returns pointers to all types of headers used by the router
 * @warning Depending on the packet type some of the headers might be invalid!!!
 * @param p packet
 * @return headers
 */
packet_headers parse_icmp_packet_headers(const packet* const p) {
    packet_headers headers = {
        .eth_header = (struct ether_header*)&p->payload[ETH_HDR_OFFSET],
        .ip_header = (struct iphdr*)&p->payload[IP_HDR_OFFSET],
        .icmp_header = (struct icmphdr*)&p->payload[ICMP_HDR_OFFSET],
        .arp_header = (struct arp_header*)&p->payload[ARP_HDR_OFFSET]};
    return headers;
}

/**
 * @brief get_interface_ip wrapper that returns a struct in_addr
 * @param interface interface number
 * @return interface in_addr
 */

struct in_addr get_interface_addr(int interface) {
    struct in_addr interface_addr;

    char* ip_str = get_interface_ip(interface);
    inet_aton(ip_str, &interface_addr);

    return interface_addr;
}

#ifdef LOG_ICMP_CONFIGS
#define DEBUG_ICMP_PACKET_CONFIG(packet_config)                                \
    debug_icmp_packet_config(packet_config)
#else
#define DEBUG_ICMP_PACKET_CONFIG(packet_config)
#endif

#ifdef LOG_ICMP_CONFIGS
void debug_icmp_packet_config(const icmp_config* const packet_config) {
    // TODO log all fields

    LOG("%s", "ICMP Packet config:");
    LOG("\tSource address: %s", inet_ntoa(packet_config->saddr));
    LOG("\tDestination address: %s", inet_ntoa(packet_config->daddr));
    LOG("\tType: %d", packet_config->type);
    LOG("\tCode: %d", packet_config->code);
    LOG("\tId: %d", ntohs(packet_config->id));
    LOG("\tSequence: %d", ntohs(packet_config->sequence));
    LOG("\tData len: %lu", packet_config->data_len);
    if (packet_config->data != NULL && packet_config->data_len != 0) {
        LOG("\tData (16 bytes): %hhx %hhx %hhx %hhx %hhx %hhx %hhx %hhx %hhx "
            "%hhx %hhx %hhx %hhx %hhx %hhx %hhx ",
            packet_config->data[0], packet_config->data[1],
            packet_config->data[2], packet_config->data[3],
            packet_config->data[4], packet_config->data[5],
            packet_config->data[6], packet_config->data[7],
            packet_config->data[8], packet_config->data[9],
            packet_config->data[10], packet_config->data[11],
            packet_config->data[12], packet_config->data[13],
            packet_config->data[14], packet_config->data[15]);
    }
}
#endif

/**
 * @brief Parse an icmp header into the internal configuratin type
 * @param p packet
 * @param config config
 */

void parse_icmp_config_from_header(packet* p, icmp_config* config) {
    packet_headers headers = parse_icmp_packet_headers(p);

    size_t icmp_data_length = ntohs(headers.ip_header->tot_len) -
                              sizeof(struct iphdr) - sizeof(struct icmphdr);
    config->code = headers.icmp_header->code;
    config->type = headers.icmp_header->type;
    config->id = headers.icmp_header->un.echo.id;
    config->sequence = headers.icmp_header->un.echo.sequence;

    struct in_addr tmp;

    tmp.s_addr = headers.ip_header->saddr;
    config->saddr = tmp;

    tmp.s_addr = headers.ip_header->daddr;
    config->daddr = tmp;

    config->data = (uint8_t*)headers.icmp_header + sizeof(struct icmphdr);
    config->data_len = icmp_data_length;

    config->interface = p->interface;
}

void liv_send_icmp(const icmp_config* const cfg);

void liv_reply_to_echo(packet* p) {
    icmp_config config;
    parse_icmp_config_from_header(p, &config);
    LOG("%s", "Replying to ICMP packet:");
    DEBUG_ICMP_PACKET_CONFIG(&config);

    struct in_addr tmp;

    tmp = config.saddr;
    config.saddr = config.daddr;
    config.daddr = tmp;

    config.type = ICMP_ECHOREPLY;

    liv_send_icmp(&config);
}

/**
 * @brief Converts icmp_config into a packet and sends it
 * @param cfg icpm configuration
 */

void liv_send_icmp(const icmp_config* const cfg) {
    LOG("%s", "Sending packet with ICMP config:");
    DEBUG_ICMP_PACKET_CONFIG(cfg);

    // Unpacking configuration
    // ==============================
    struct in_addr daddr = cfg->daddr;
    struct in_addr saddr = cfg->saddr;
    uint8_t type = cfg->type;
    uint8_t code = cfg->code;
    uint16_t id = cfg->id;
    uint16_t sequence = cfg->sequence;
    void* data = cfg->data;
    size_t data_len = cfg->data_len;
    // ==============================

    packet p = {0};

    packet_headers headers = parse_icmp_packet_headers(&p);

    headers.eth_header->ether_type = htons(ETHERTYPE_IP);

    struct iphdr* ip = headers.ip_header;
    struct icmphdr* icmp = headers.icmp_header;
    void* data_ptr = ((uint8_t*)icmp) + sizeof(struct icmphdr);

    // Setup icmp header
    icmp->code = code;
    icmp->type = type;

    if (cfg->type == ICMP_ECHOREPLY) {
        icmp->un.echo.id = id;
        icmp->un.echo.sequence = sequence;
    }
    icmp->checksum = 0;
    // Setup ip header
    ip->version = IPVERSION;
    ip->saddr = saddr.s_addr;
    ip->daddr = daddr.s_addr;
    ip->frag_off = 0;
    ip->protocol = IPPROTO_ICMP;
    ip->tos = 0;
    ip->ihl = (sizeof(struct iphdr)) / 4;
    ip->ttl = 64;
    ip->tot_len = htons(
        (uint16_t)(sizeof(struct iphdr) + sizeof(struct icmphdr) + data_len));
    ip->check = 0;

    memcpy(data_ptr, data, data_len);

    if (cfg->type == ICMP_ECHOREPLY) {
        *((time_t*)data_ptr) = time(NULL);
    }

    icmp->checksum = (icmp_checksum(
        (uint16_t*)icmp, (uint32_t)(sizeof(struct icmphdr) + data_len)));
    ip->check = ip_checksum(ip, sizeof(struct iphdr));
    INFO("%s", "Sending icmp packet...");

    struct in_addr dest_addr = {.s_addr = ip->daddr};
    const route_entry* const best_route = get_best_route(dest_addr);

    p.len = (int)(sizeof(struct ether_header) + sizeof(struct iphdr) +
                  sizeof(struct icmphdr) + data_len);

    liv_send_packet(&p, best_route);
}

/*
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
