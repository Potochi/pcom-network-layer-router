# Overview

Programul ruleaza o bucla infinita in care se intampla urmatoarele:
	- Citeste un pachet de pe oricare interfata
	- Verifica daca este un pachet de tip arp si efectueaza urmatoarele actiuni
		- ARP_REQUEST: verifica daca cererea este pentru o adresa mac care apartine
					   routerului, iar in cazul in care este, raspunde cu un ARP_REPLY
					   cu adresa ceruta
		- ARP_REPLY:   adauga adresa primita in tabela arp, iar in cazul in care exista
					   pachete care nu au fost trimise din cauza lipsei adresei mac.
	- Verifica daca TTL-ul pachetului este mai mic sau egal cu 1, iar daca este cazul,
	  arunca pachetul expirat si trimite un pachet ICMP Time To Live Exceeded catre sursa
	  pachetului expirat.
	- Verifica daca checksum-ul headerului IP este valid, in cazul in care nu este arunca
	  pachetul
	- Verifica daca este un pachet de tip ICMP Echo Request destinat routerului, in caz
	  afirmativ verifica checksumul headerului ICMP, daca checksumul este invalid arunca 
	  pachetul, iar daca este valid, raspunde cu un Echo Reply.
	- Decrementeaza TTL-ul si recalculeaza checksumul pachetului folosind algoritmul
	  incremental RFC 1624.
	- In cazul in care nu se gaseste destinatia in routing table, se trimite sursei un 
	  pachet ICMP Destination Host Unreachable sursei si se arunca pachetul.
	- In cazul in care pachetul nu cade in cazurile de mai sus si nu a fost aruncat 
	  este trimis catre urmatorul hop.
	
# Routing table

Pentru a satisface cerinta de cautare a best-route-ului in O(log n), am ales sa folosesc
o trie, in care au fost stocate entry-urile din fisier dupa adresa de retea, astfel
se realizeaza cautarea dupa masca maximala.

# Arp table

Pentru rezolvarea queryurilor pentru adrese mac, am folosit un array cu marime dinamica,
astfel realizand query-uri in O(n)

# My icmp

Deoarece functia din schelet pentru trimiterea de pachete ICMP nu avea optiunea de a trimite
date in pachet ( fapt care cauza pachetele ICMP Echo Reply primite de utilitarul ping sa 
nu fie recunoscute din cauza lipsei datelor din pachet), am ales sa implementez o functie 
care suporta si trimiterea de date.
