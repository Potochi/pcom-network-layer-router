#pragma once

#include <stdint.h>
#include <stdlib.h>

#define COLOR_PREFIX "\x1B["
#define COLOR_SEP ";"
#define COLOR_SUFFIX "m"

#define COLOR_BLACK "0"
#define COLOR_RED "1"
#define COLOR_GREEN "2"
#define COLOR_YELLOW "3"
#define COLOR_BLUE "4"
#define COLOR_MAGENTA "5"
#define COLOR_CYAN "6"
#define COLOR_WHITE "7"

#define COLOR_FOREGROUND "3"
#define COLOR_BACKGROUND "4"

#define COLOR_SGR_CODE(number) COLOR_SEP #number
#define COLOR_BRIGHT COLOR_SGR_CODE(1)
#define COLOR_UNDERLINE COLOR_SGR_CODE(4)
#define COLOR_BLINK COLOR_SGR_CODE(5)
#define COLOR_REVERSE COLOR_SGR_CODE(7)
#define COLOR_INVISIBLE COLOR_SGR_CODE(8)

#define COLOR_RESET COLOR_PREFIX "0" COLOR_SUFFIX

#define COLOR_ ""

#define SET_ATTR8(attr, ...) COLOR_##attr
#define SET_ATTR7(attr, ...) COLOR_##attr SET_ATTR8(__VA_ARGS__)
#define SET_ATTR6(attr, ...) COLOR_##attr SET_ATTR7(__VA_ARGS__)
#define SET_ATTR5(attr, ...) COLOR_##attr SET_ATTR6(__VA_ARGS__)
#define SET_ATTR4(attr, ...) COLOR_##attr SET_ATTR5(__VA_ARGS__)
#define SET_ATTR3(attr, ...) COLOR_##attr SET_ATTR4(__VA_ARGS__)
#define SET_ATTR2(attr, ...) COLOR_##attr SET_ATTR3(__VA_ARGS__)
#define SET_ATTR(attr, ...) COLOR_##attr SET_ATTR2(__VA_ARGS__)

#define ANSI_COLOR_FG(forecolor, ...)                                          \
    COLOR_PREFIX                                                               \
    COLOR_FOREGROUND COLOR_##forecolor SET_ATTR(__VA_ARGS__) COLOR_SUFFIX

#define ANSI_COLOR_FGBG(forecolor, backcolor, ...)                             \
    COLOR_PREFIX                                                               \
    COLOR_FOREGROUND COLOR_##forecolor COLOR_SEP COLOR_BACKGROUND              \
        COLOR_##backcolor SET_ATTR(__VA_ARGS__) COLOR_SUFFIX

#define COLORIZEFG(text, forecolor, ...)                                       \
    ANSI_COLOR_FG(forecolor, __VA_ARGS__) text COLOR_RESET
#define COLORIZEFGBG(text, forecolor, backcolor, ...)                          \
    ANSI_COLOR_FGBG(forecolor, backcolor, __VA_ARGS__) text COLOR_RESET

#define LOGGING_LEVEL -1

#define LOG_LVL 0
#define INFO_LVL 1
#define WARN_LVL 2
#define ERROR_LVL 3
#define CRITICAL_LVL 4

/**
  @brief Uncomment these macros to enable logging of all icmp/arp packets
  received and sent
  */
//#define LOG_ICMP_CONFIGS
//#define LOG_ARP_CONFIGS
//#define LOG_RTABLE_ENTRIES
//#define LOG_RTABLE_QUERIES
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

#define LOG(fmt, ...)                                                          \
    do {                                                                       \
        if (LOGGING_LEVEL >= LOG_LVL)                                          \
            printf(COLORIZEFG("[LOG]"                                          \
                              "<"__FILE__                                      \
                              ":%s:" TOSTRING(__LINE__) "> ",                  \
                              GREEN) COLORIZEFG(fmt, GREEN) "\n",              \
                   __PRETTY_FUNCTION__, __VA_ARGS__);                          \
    } while (0)

#define INFO(fmt, ...)                                                         \
    do {                                                                       \
        if (LOGGING_LEVEL >= LOG_LVL)                                          \
            printf(COLORIZEFG("[INFO]"                                         \
                              "<"__FILE__                                      \
                              ":%s:" TOSTRING(__LINE__) "> ",                  \
                              BLUE, BRIGHT)                                    \
                       COLORIZEFG(fmt, BLUE, BRIGHT) "\n",                     \
                   __PRETTY_FUNCTION__, __VA_ARGS__);                          \
    } while (0)
#define WARN(fmt, ...)                                                         \
    do {                                                                       \
        if (LOGGING_LEVEL >= LOG_LVL)                                          \
            printf(COLORIZEFG("[WARN]"                                         \
                              "<"__FILE__                                      \
                              ":%s:" TOSTRING(__LINE__) "> ",                  \
                              YELLOW, BRIGHT)                                  \
                       COLORIZEFG(fmt, YELLOW, BRIGHT) "\n",                   \
                   __PRETTY_FUNCTION__, __VA_ARGS__);                          \
    } while (0)
#define ERROR(fmt, ...)                                                        \
    do {                                                                       \
        if (LOGGING_LEVEL >= LOG_LVL)                                          \
            printf(COLORIZEFG("[ERROR]"                                        \
                              "<"__FILE__                                      \
                              ":%s:" TOSTRING(__LINE__) "> ",                  \
                              RED, BRIGHT) COLORIZEFG(fmt, RED, BRIGHT) "\n",  \
                   __PRETTY_FUNCTION__, __VA_ARGS__);                          \
    } while (0)
#define CRITICAL(fmt, ...)                                                     \
    do {                                                                       \
        if (LOGGING_LEVEL >= LOG_LVL)                                          \
            printf(COLORIZEFGBG("[CRITICAL]"                                   \
                                "<"__FILE__                                    \
                                ":%s:" TOSTRING(__LINE__) "> ",                \
                                YELLOW, RED, BRIGHT, BLINK)                    \
                       COLORIZEFGBG(fmt, YELLOW, RED, BRIGHT) "\n",            \
                   __PRETTY_FUNCTION__, __VA_ARGS__);                          \
    } while (0)

#define assert_true(condition, message)                                        \
    do {                                                                       \
        if (!(condition)) {                                                    \
            CRITICAL("%s => %s", "ASSERTION FAIL!", message);                  \
            exit(-1);                                                          \
        }                                                                      \
                                                                               \
    } while (0)

#define assert_false(condition, message)                                       \
    do {                                                                       \
        if ((condition)) {                                                     \
            CRITICAL("%s => %s", "ASSERTION FAIL!", message);                  \
            exit(-1);                                                          \
        }                                                                      \
                                                                               \
    } while (0)

#define assert_not_reached(message)                                            \
    do {                                                                       \
        CRITICAL("%s => %s", "ASSERTION FAIL!", message);                      \
        exit(-1);                                                              \
    } while (0)

/*
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
