#include "arp_table.h"
#include "liblog.h"
#include "queue.h"
#include <arpa/inet.h>
#include <skel.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct arp_table_t {
    arp_table_entry* entries;
    size_t capacity;
    size_t size;
    uint32_t initialized;
} arp_table;

arp_table* table;

void arp_table_init() {
    INFO("Initialising ARP table for %u entries...", ROUTER_NUM_INTERFACES);
    table = calloc(1, sizeof(arp_table));
    table->initialized = 1;
    table->capacity = ROUTER_NUM_INTERFACES;
    table->size = ROUTER_NUM_INTERFACES;

    table->entries = calloc(ROUTER_NUM_INTERFACES, sizeof(*table->entries));

    assert_true(table->entries != NULL,
                "Failed to allocate memory for ARP table entries");
    struct in_addr tmp;
    uint8_t mac[6];

    for (int i = 0; i < ROUTER_NUM_INTERFACES; i++) {
        char* ip = get_interface_ip(i);

        assert_true(ip != NULL, "Failed to get interface IP");
        get_interface_mac(i, mac);

        assert_true(inet_aton(ip, &tmp) != 0,
                    "Failed conversion from ip string to number");

        LOG("Adding ARP entry %s -> %hhx:%hhx:%hhx:%hhx:%hhx:%hhx", ip, mac[0],
            mac[1], mac[2], mac[3], mac[4], mac[5]);

        table->entries[i].has_mac = 1;
        table->entries[i].ip_addr = tmp;
        table->entries[i].send_queue = NULL;
        memcpy(table->entries[i].mac, mac, sizeof(table->entries[i].mac));
    }
    INFO("%s", "ARP table initialisation done.");
}

arp_table_entry* arp_table_find_mac(struct in_addr addr) {
    assert_true(
        table != NULL && table->initialized == 1,
        "Trying to add entries to uninitialised ARP table. Aborting...");

    for (size_t i = 0; i < table->size; i++) {

        INFO("%s", "Comparing addresses: ");
        INFO("\tNeedle: %s", inet_ntoa(addr));
        INFO("\tHaystack: %s has_mac: %d", inet_ntoa(table->entries[i].ip_addr),
             table->entries[i].has_mac);

        if (table->entries[i].ip_addr.s_addr == addr.s_addr) {
            return &table->entries[i];
        }
    }
    return NULL;
}

queue arp_table_set_mac(struct in_addr addr, uint8_t* mac) {
    arp_table_entry* entry = arp_table_find_mac(addr);

    assert_true(entry != NULL, "Trying to set mac on non requested address.");

    if (entry->has_mac == 1) {
        return queue_create();
    }

    assert_true(entry->has_mac != 1,
                "Trying to set mac on address that already has mac.");

    memcpy(&entry->mac[0], mac, sizeof(entry->mac));

    entry->has_mac = 1;
    queue packet_queue = entry->send_queue;
    entry->send_queue = NULL;

    return packet_queue;
}

void arp_table_enqueue_packet(packet* m, struct in_addr addr) {

    arp_table_entry* entry = arp_table_find_mac(addr);

    assert_true(
        entry == NULL || entry->has_mac == 0,
        "Trying to enqueue packets of an address that we know. Aborting...");

    INFO("Enqueueing packet for address: %s", inet_ntoa(addr));

    packet* copy = calloc(1, sizeof(packet));
    memcpy(copy, m, sizeof(packet));

    if (entry == NULL) {
        if (table->capacity == table->size) {
            INFO("%s", "Reallocating ARP entries array");
            table->capacity = 2 * table->capacity + 1;
            table->entries = realloc(table->entries, (sizeof(arp_table_entry)) *
                                                         table->capacity);
            assert_true(table->entries != NULL,
                        "Failed to allocate memory for table entries");
        }
        INFO("%s", "Creating new queue");

        table->entries[table->size].ip_addr = addr;
        table->entries[table->size].send_queue = queue_create();
        queue_enq(table->entries[table->size].send_queue, copy);
        table->entries[table->size].has_mac = 0;

        table->size += 1;
    } else {
        INFO("%s", "Queue already exists, adding...");
        queue_enq(entry->send_queue, copy);
    }
}
