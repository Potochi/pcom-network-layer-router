#pragma once
#include "queue.h"
#include "skel.h"
#include <arpa/inet.h>
#include <stdint.h>

typedef struct arp_table_entry_t {
    struct in_addr ip_addr;
    uint8_t mac[6];
    uint32_t has_mac;
    queue send_queue;
} arp_table_entry;

void arp_table_init();

arp_table_entry* arp_table_find_mac(struct in_addr addr);

queue arp_table_set_mac(struct in_addr addr, uint8_t* mac);

void arp_table_enqueue_packet(packet* m, struct in_addr addr);
