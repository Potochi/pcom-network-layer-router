#include "router.h"
#include "arp_table.h"
#include "liblog.h"
#include "my_icmp.h"
#include "routing_table.h"
#include "skel.h"
#include <errno.h>
#include <queue.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

static const uint8_t mac_broadcast[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};

/**
 * @brief Handles an arp reply and sends previously enqueued packets for that
 * address
 * @param arp_header arp header of the reply
 */
static void handle_arp_reply(struct arp_header* arp_header) {
#ifdef LOG_ARP_CONFIGS
    struct in_addr tmp;
    LOG("%s", "ARP reply received:");
    tmp.s_addr = arp_header->spa;
    LOG("\tsource_ip_addr: %s", inet_ntoa(tmp));
    LOG("\tsource_mac_addr: %hhx:%hhx:%hhx:%hhx:%hhx:%hhx", arp_header->sha[0],
        arp_header->sha[1], arp_header->sha[2], arp_header->sha[3],
        arp_header->sha[4], arp_header->sha[5]);
    LOG("\ttarget_mac_addr: %hhx:%hhx:%hhx:%hhx:%hhx:%hhx", arp_header->tha[0],
        arp_header->tha[1], arp_header->tha[2], arp_header->tha[3],
        arp_header->tha[4], arp_header->tha[5]);
    tmp.s_addr = arp_header->tpa;
    LOG("\ttarget_ip_addr: %s", inet_ntoa(tmp));
#endif

    struct in_addr dest_addr;
    dest_addr.s_addr = arp_header->spa;
    queue q = arp_table_set_mac(dest_addr, arp_header->sha);

    if (q != NULL) {
        LOG("%s", "Sending packets from queue");

        while (!queue_empty(q)) {
            packet* p = queue_deq(q);
            INFO("%s", "Packet from queue sent");

            struct iphdr* ip =
                (struct iphdr*)&p->payload[sizeof(struct ether_header)];
            struct in_addr lel = {.s_addr = ip->daddr};
            const route_entry* best = get_best_route(lel);

            liv_send_packet(p, best);
            free(p);
        }
        free(q);
        return;
    }
    LOG("%s", "No packets in queue");
}

/**
 * @brief Wrapper around send_packet that handles arp resolution
 * @param p packet to send
 * @param best_route best route to destination
 */
void liv_send_packet(packet* p, const route_entry* const best_route) {
    struct ether_header* eth = (struct ether_header*)&p->payload[0];
    get_interface_mac(best_route->interface, eth->ether_shost);

    if (best_route == NULL) {
        send_packet(p->interface, p);
        return;
    }

    struct in_addr router_ip;
    inet_aton(get_interface_ip(best_route->interface), &router_ip);

    const arp_table_entry* const arp_entry =
        arp_table_find_mac(best_route->next_hop);

    if (arp_entry == NULL || arp_entry->has_mac == 0) {
        INFO("ARP resolution failed for ip: %s , sending an ARP request and "
             "enqueueing packet",
             inet_ntoa(best_route->next_hop));

        struct ether_header arp_ether = {.ether_type = htons(ETHERTYPE_ARP)};
        get_interface_mac(best_route->interface, arp_ether.ether_shost);

        memcpy(arp_ether.ether_dhost, mac_broadcast,
               sizeof(arp_ether.ether_dhost));

        arp_table_enqueue_packet(p, best_route->next_hop);

        send_arp(best_route->next_hop.s_addr, router_ip.s_addr, &arp_ether,
                 best_route->interface, htons(ARPOP_REQUEST));

        return;
    }

    INFO("%s", "ARP resolution succeded, sending packet");

    memcpy(eth->ether_dhost, arp_entry->mac, sizeof(eth->ether_dhost));
    INFO("%s %d", "Sending packet in interface", best_route->interface);

    INFO("Packet ptr: %p", p);
    send_packet(best_route->interface, p);
}

/**
 * @brief Checks if the provided ip address matches any address on the router's
 * interfaces
 * @param addr address to check
 * @return 1 if true, 0 if false
 */
static int is_router_addr(uint32_t addr) {
    struct in_addr router_ip;
    for (size_t i = 0; i < ROUTER_NUM_INTERFACES; i++) {
        int result = inet_aton(get_interface_ip((int)i), &router_ip);
        assert_true(result != 0,
                    "Failed to convert router ip string to number");
        if (router_ip.s_addr == addr) {
            return 1;
        }
    }
    return 0;
}
/**
 * @brief Handles an arp request and responds with a reply if the arp request
 * is for one of the router's interfaces
 * @param p received packet
 * @param arp_header received packet's arp header
 */
static void handle_arp_request(packet* p, struct arp_header* arp_header) {

    struct ethhdr* eth_header = (struct ethhdr*)&p->payload[0];

#ifdef LOG_ARP_CONFIGS
    struct in_addr tmp;
    tmp.s_addr = arp_header->spa;

    LOG("%s", "ARP request received:");
    tmp.s_addr = arp_header->spa;
    LOG("\tsource_ip_addr: %s", inet_ntoa(tmp));
    LOG("\tsource_mac_addr: %hhx:%hhx:%hhx:%hhx:%hhx:%hhx", arp_header->sha[0],
        arp_header->sha[1], arp_header->sha[2], arp_header->sha[3],
        arp_header->sha[4], arp_header->sha[5]);
    LOG("\ttarget_mac_addr: %hhx:%hhx:%hhx:%hhx:%hhx:%hhx", arp_header->tha[0],
        arp_header->tha[1], arp_header->tha[2], arp_header->tha[3],
        arp_header->tha[4], arp_header->tha[5]);
    tmp.s_addr = arp_header->tpa;
    LOG("\ttarget_ip_addr: %s", inet_ntoa(tmp));
#endif

    if (is_router_addr(arp_header->tpa)) {
        LOG("%s", "Request is for router");

        struct in_addr router_ip;
        inet_aton(get_interface_ip(p->interface), &router_ip);

        struct ether_header ether_header;

        get_interface_mac(p->interface, ether_header.ether_shost);
        memcpy(ether_header.ether_dhost, eth_header->h_source,
               sizeof(ether_header.ether_dhost));

        ether_header.ether_type = htons(ETHERTYPE_ARP);

        send_arp(arp_header->spa, arp_header->tpa, &ether_header, p->interface,
                 htons(ARPOP_REPLY));

    } else {
        LOG("%s", "Request not for router");
    }
}

int main(int argc, char* argv[]) {

    setbuf(stdout, NULL);
    packet m;
    int rc;

    init(argc - 2, argv + 2);

    const char* rtable_filename = argv[1];
    routing_table_init(rtable_filename);
    arp_table_init();

    while (1) {
        rc = get_packet(&m);
        assert_true(rc >= 0, "Failed to receive message. Aborting...");

        INFO("%s", "Received packet...");
        struct iphdr* ip_header =
            (struct iphdr*)&m.payload[sizeof(struct ether_header)];

        struct arp_header* arp_header = parse_arp(&m.payload);

        struct icmphdr* icmp_header = parse_icmp(&m.payload);

        struct in_addr router;
        inet_aton(get_interface_ip(m.interface), &router);

        if (arp_header != NULL) {

            LOG("%s", "Received an ARP message");
            switch (ntohs(arp_header->op)) {
            case 1: /* ARP Request */ {
                handle_arp_request(&m, arp_header);
            } break;
            case 2: /* ARP Reply */ {
                handle_arp_reply(arp_header);
            } break;
            default:
                assert_not_reached("Invalid ARP operation type. Aborting...");
            }

            continue;
        }
        uint8_t mac[6];
        get_interface_mac(m.interface, &mac[0]);

        if (ip_header->ttl <= 1) {
            LOG("%s", "Packet with TTL exired");
            icmp_config config = {.type = 11,
                                  .code = ICMP_EXC_TTL,
                                  .daddr = {.s_addr = ip_header->saddr},
                                  .saddr = router,
                                  .data_len = 64,
                                  .data = (uint8_t*)ip_header,
                                  .interface = m.interface};
            liv_send_icmp(&config);

            continue;
        }

        if (ip_checksum(ip_header, sizeof(struct iphdr)) != 0) {
            LOG("%s", "Wrong IP packet checksum");
            continue;
        }

        if (icmp_header != NULL && is_router_addr(ip_header->daddr) &&
            icmp_header->type == ICMP_ECHO) {
            LOG("%s", "Got ICMP packet for router\n");

            icmp_config config;

            parse_icmp_config_from_header(&m, &config);

            if (icmp_checksum(
                    (uint16_t*)icmp_header,
                    (uint32_t)(sizeof(icmp_header) + config.data_len)) != 0) {
                LOG("%s", "ICMP checksum failed");
                continue;
            }

            liv_reply_to_echo(&m);
            continue;
        }

        ip_header->check = (uint16_t)(
            ~(~(ip_header->check) + -ip_header->ttl + (ip_header->ttl - 1)));
        ip_header->ttl -= 1;

        struct in_addr dest = {.s_addr = ip_header->daddr};
        const route_entry* const best_route = get_best_route(dest);

        if (best_route == NULL) {
            icmp_config config = {.code = 0,
                                  .type = 3,
                                  .interface = m.interface,
                                  .data = (uint8_t*)ip_header,
                                  .data_len = 64,
                                  .daddr = {.s_addr = ip_header->saddr},
                                  .saddr = router};

            liv_send_icmp(&config);

            continue;
        }

        liv_send_packet(&m, best_route);
    }
}
